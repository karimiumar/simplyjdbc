package com.umar.simply.jdbc;

import java.sql.SQLException;

import javax.sql.RowSet;

public interface RowSetMapper<T> {
	/**
     * Map the given SQL ResultSet to the corresponding type T
     * @param rowSet The javax.sql.RowSet to map
     * @return Returns the type mapped
     * @throws SQLException 
     */
	T map(RowSet rowSet) throws SQLException;
}
